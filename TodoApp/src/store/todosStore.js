import create from "zustand";

const useTodosStore = (set) => ({
  todos: [],
  addTodo: (title) =>
    set((state) => ({
      todos: [
        ...state.todos,
        { title, id: Math.floor(Math.random() * 1000), isCompleted: false },
      ],
    })),
  toggleTodo: (id) =>
    set((state) => {
      let cloned = [...state.todos];
      const itemIndex = cloned.findIndex((item) => item.id === id);
      cloned[itemIndex].isCompleted = !cloned[itemIndex].isCompleted;
      return {
        todos: cloned,
      };
    }),
});

export default create(useTodosStore);

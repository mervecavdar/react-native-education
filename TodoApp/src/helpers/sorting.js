export const sortData = (data, sortBy) => {
  if (!data) {
    return [];
  }

  return data.sort((a, b) => {
    if (a.title < b.title) {
      return sortBy === "asc" ? -1 : 1;
    }
    if (a.title > b.title) {
      return sortBy === "asc" ? 1 : -1;
    }

    return 0;
  });
};

import { useState } from "react";
import { View, Text, StyleSheet, FlatList, Button } from "react-native";
import { sortData } from "../../helpers/sorting";
import useTodosStore from "../../store/todosStore";
import Stats from "../Stats";
import TodoItem from "./TodoItem";

const Todos = () => {
  const [sortBy, setSortBy] = useState("asc");
  const todos = useTodosStore((state) => state.todos);

  const sorted = sortData(todos, sortBy);

  return (
    <View style={styles.container}>
      <View style={styles.head}>
        <View style={styles.head_inner}>
          <Text style={styles.title}>Todos</Text>
          <View style={styles.btn}>
            <Button
              title="asc"
              onPress={() => setSortBy("asc")}
              color={sortBy === "asc" ? "red" : "black"}
            />
            <Button
              title="desc"
              onPress={() => setSortBy("desc")}
              color={sortBy === "desc" ? "red" : "black"}
            />
          </View>
        </View>
        <Stats />
      </View>
      <FlatList
        data={sorted}
        renderItem={({ item }) => <TodoItem item={item} />}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 34,
    margin: 10,
  },
  head: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  head_inner: {
    flexDirection: "row",
  },
  btn: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default Todos;

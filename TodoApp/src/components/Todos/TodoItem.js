import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import useTodosStore from "../../store/todosStore";

const TodoItem = ({ item }) => {
  const toggleTodo = useTodosStore((state) => state.toggleTodo);

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => toggleTodo(item.id)}
    >
      <Text style={styles.text}>{item.title}</Text>
      <Ionicons name={item.isCompleted ? "checkbox" : "square"} size={24} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "#f8f8f8",
    borderBottomWidth: 1,
    borderBottomColor: "#e9e9e9",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  text: {
    fontSize: 18,
  },
});

export default TodoItem;

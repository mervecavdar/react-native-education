import React, { useState } from "react";
import { View, StyleSheet, TextInput } from "react-native";
import useTodosStore from "../../store/todosStore";

const Form = () => {
  const [title, setTitle] = useState("");
  const addTodo = useTodosStore((state) => state.addTodo);

  const handleAddItem = () => {
    if (!title) {
      return;
    }
    addTodo(title);
    setTitle("");
  };

  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="Add new task.."
        returnKeyType="done"
        onEndEditing={handleAddItem}
        onChangeText={setTitle}
        value={title}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderColor: "#666",
    padding: 10,
    fontSize: 18,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
});

export default Form;

import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import useTodosStore from "../../store/todosStore";

const Stats = () => {
  const todos = useTodosStore((state) => state.todos);
  const completedTasksLength = todos.filter((item) => item.isCompleted).length;

  return (
    <View style={styles.container}>
      <View style={styles.item}>
        <Ionicons name={"checkbox"} size={22} />
        <Text style={styles.text}>{completedTasksLength}</Text>
      </View>
      <View style={styles.item}>
        <Ionicons name={"square"} size={22} />
        <Text style={styles.text}>{todos.length - completedTasksLength}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
    flexDirection: "row",
  },
  item: {
    alignItems: "center",
    paddingHorizontal: 4,
  },
  text: {
    fontSize: 18,
  },
});

export default Stats;

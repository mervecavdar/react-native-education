import { useState } from "react";
import { StyleSheet, SafeAreaView, Button } from "react-native";
import ListItem from "./src/components/ListItem";
import Counter from "./src/components/Counter";
import Fetching from "./src/components/Fetching";
import List from "./src/components/List";

export default function App() {
  const [isVisible, setIsVisible] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <ListItem name={"Jack"} />
      {isVisible ? <Counter /> : null}
      <Button
        title={isVisible ? "Hide" : "Show"}
        onPress={() => setIsVisible(!isVisible)}
      />
      <Fetching />
      <List />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});

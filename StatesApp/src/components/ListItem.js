import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

const ListItem = ({ name }) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={styles.title}>{name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#999",
  },
  title: {
    fontSize: 22,
  },
});

export default ListItem;

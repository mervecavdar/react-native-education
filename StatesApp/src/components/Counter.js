import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const Counter = () => {
  const [count, setCount] = useState(0);
  const [amount, setAmount] = useState(2);

  const increase = () => {
    setCount(count + amount);
  };

  const decrease = () => {
    setCount(count - amount);
  };

  useEffect(() => {
    console.log("Mount!");

    const interval = setInterval(() => {
      console.log("Interval!");
      setCount((prev) => prev + 1);
    }, 1000);

    return () => {
      console.log("Unmount!");
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    console.log("Amount state value changed!");
  }, [amount]);

  useEffect(() => {
    console.log("Count or amount state value changed!");
  }, [count, amount]);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{count}</Text>
      <Button title="Increase" onPress={increase} />
      <Button title="Decrease" onPress={decrease} />
      <Text style={styles.amountTitle}>Amount: {amount}</Text>
      <View style={styles.amountBtn}>
        <Button title="+1" onPress={() => setAmount(1)} />
        <Button title="+5" onPress={() => setAmount(5)} />
        <Button title="+10" onPress={() => setAmount(10)} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 30,
  },
  amountBtn: {
    flexDirection: "row",
  },
  amountTitle: {
    paddingTop: 20,
    fontSize: 18,
  },
});

export default Counter;

import { useState } from "react";
import { View, StyleSheet, Text, ScrollView, Button } from "react-native";
import useFetch from "../hooks/useFetch";

const Fetching = () => {
  const URL = "https://jsonplaceholder.typicode.com/users";

  const [url, setUrl] = useState(URL + "/1");
  const { data, error, loading } = useFetch(url);

  if (loading) {
    return <Text style={styles.loading}>Loading...</Text>;
  }

  if (error) {
    return <Text>{error}</Text>;
  }

  return (
    <View style={styles.container}>
      <Button title="Change Endpoint" onPress={() => setUrl(URL + "/2")} />
      <ScrollView>
        <Text>{JSON.stringify(data, null, 2)}</Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e8e8e8",
  },
  loading: {
    fontSize: 22,
    padding: 20,
  },
});

export default Fetching;

import React, { useState } from "react";
import { View, Text, Button, StyleSheet, TextInput } from "react-native";

const List = () => {
  const [users, setUsers] = useState([
    { name: "Ahmet" },
    { name: "Ayşe" },
    { name: "Murat" },
  ]);

  const [name, setName] = useState("");

  const handleAdd = () => {
    setUsers((prev) => [...prev, { name }]);
    setName("");
  };

  const handleRemove = (index) => {
    const cloned = [...users];
    cloned.splice(index, 1);
    setUsers([...cloned]);
  };

  return (
    <View style={styles.container}>
      {!users.length && <Text>Not Found!</Text>}
      {users.map((user, index) => (
        <View key={index} style={styles.list_container}>
          <Text style={styles.title}>
            {index + 1} - {user.name}
          </Text>
          <Button title="Remove" onPress={() => handleRemove(index)} />
        </View>
      ))}
      <TextInput
        style={styles.input}
        keyboardType="numeric"
        value={name}
        onChangeText={(text) => setName(text)}
      />
      <Button title="Add" onPress={handleAdd} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginHorizontal: 20,
    flexDirection: "column",
  },
  list_container: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
  },
  title: {
    fontSize: 18,
  },
  input: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    width: "100%",
    marginTop: 20,
  },
});

export default List;

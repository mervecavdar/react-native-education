import React from "react";
import { Marker } from "react-native-maps";
import useMapStore from "../store/useMapStore";

const Markers = () => {
  const markers = useMapStore((state) => state.markers);

  return (
    <>
      {markers.map((marker) => (
        <Marker
          key={marker.id}
          coordinate={marker.coordinate}
          title={marker.title}
          description={marker.description}
        />
      ))}
    </>
  );
};

export default Markers;

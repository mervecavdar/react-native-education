import React, { useState } from "react";
import { View, StyleSheet, Button, TextInput } from "react-native";
import useMapStore from "../store/useMapStore";

const ModalContent = ({ setModalVisible, selectedCoordinate }) => {
  const addMarker = useMapStore((state) => state.addMarker);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const handleAdd = () => {
    if (!title) {
      return false;
    }
    addMarker({ coordinate: selectedCoordinate, title, description });
    setModalVisible(false);
  };

  return (
    <View style={styles.content}>
      <TextInput
        style={styles.input}
        placeholder="Title"
        value={title}
        onChangeText={setTitle}
      />
      <TextInput
        style={styles.input}
        placeholder="Description"
        multiline
        value={description}
        onChangeText={setDescription}
      />

      <View style={styles.btn}>
        <Button testID={"close-button"} onPress={handleAdd} title="Add" />
        <Button
          testID={"close-button"}
          onPress={() => setModalVisible(false)}
          title="Close"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
  input: {
    width: "100%",
    borderBottomWidth: 1,
    borderColor: "#999",
    padding: 10,
    marginBottom: 12,
    fontSize: 18,
  },
  btn: {
    flexDirection: "row",
  },
});

export default ModalContent;

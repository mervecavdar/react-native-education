import React from "react";
import { View, StyleSheet, Button } from "react-native";
import useMapStore from "../store/useMapStore";

const Overlay = () => {
  const addMarker = useMapStore((state) => state.addMarker);

  const handlePress = () => {
    addMarker({
      title: "Title",
      description: "Description",
      coordinate: {
        latitude: 41.016721,
        longitude: 28.973718,
      },
    });
  };

  return (
    <View style={styles.overlay}>
      <View style={styles.overlay_inner}>
        <Button title="Title" onPress={handlePress} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  overlay: {
    position: "absolute",
    bottom: 100,
    left: 0,
    width: "100%",
  },
  overlay_inner: {
    backgroundColor: "#e8e8e8",
    flex: 1,
  },
});

export default Overlay;

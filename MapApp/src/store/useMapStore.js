import create from "zustand";

const initialMarkers = [
  {
    id: 1,
    coordinate: {
      latitude: 41.017595,
      longitude: 28.971691,
    },
    title: "Title1",
    description: "Description2",
  },
  {
    id: 2,
    coordinate: {
      latitude: 41.017133,
      longitude: 28.975832,
    },
    title: "Title2",
    description: "Description2",
  },
];

const useMapStore = (set) => ({
  markers: [...initialMarkers],
  addMarker: (data) =>
    set((state) => ({
      markers: [{ ...data, id: ~~(Math.random() * 1000) }, ...state.markers],
    })),
});

export default create(useMapStore);

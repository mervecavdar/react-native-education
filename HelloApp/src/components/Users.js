import React from "react";
import { View } from "react-native";
import User from "./User";

const users = [
  {
    id: 1,
    name: "Kaan",
    age: 20,
    isAdmin: true,
    superAdmin: false,
  },
  {
    id: 2,
    name: "Fatma",
    age: 30,
    isAdmin: true,
    superAdmin: true,
  },
  {
    id: 3,
    name: "Yağmur",
    age: 40,
    isAdmin: false,
    superAdmin: false,
  },
];

const Users = () => {
  return (
    <View>
      {users.map((user) => (
        <User {...user} key={user.id} />
      ))}
    </View>
  );
};

export default Users;

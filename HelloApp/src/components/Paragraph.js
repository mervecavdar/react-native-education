import React from "react";
import { Text, View } from "react-native";

const Paragraph = (props) => {
  return (
    <View>
      <Text>{props.text}</Text>
    </View>
  );
};

export default Paragraph;

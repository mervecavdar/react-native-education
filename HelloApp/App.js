import {
  View,
  StyleSheet,
  SafeAreaView,
  Platform,
  StatusBar,
} from "react-native";
import Avatar from "./src/components/Avatar";
import Paragraph from "./src/components/Paragraph";
import User from "./src/components/User";
import Users from "./src/components/Users";

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.box}>
        <Avatar />
        <Paragraph text={"Paragraph Text"} />
        <User name={"Kaan"} age={20} />
        <Users />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "yellow",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});

export default App;

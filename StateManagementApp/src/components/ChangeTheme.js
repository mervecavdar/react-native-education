import { View, Text, StyleSheet, Button } from "react-native";
import { useTheme } from "../context/ThemeContext";

const ChangeTheme = () => {
  const { theme, setTheme } = useTheme();

  return (
    <View>
      <Text style={styles.text}>Active Theme: {theme} </Text>
      <Button
        title="Change Theme"
        onPress={() => setTheme((prev) => (prev === "dark" ? "light" : "dark"))}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
  },
});

export default ChangeTheme;

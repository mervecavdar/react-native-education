import { View, Button, StyleSheet } from "react-native";
import { useLang } from "../context/LangContext";

const ChangeLanguage = () => {
  const { lang, setLang } = useLang();

  return (
    <View style={styles.container}>
      <Button
        title="en-US"
        onPress={() => setLang("en-US")}
        color={lang === "en-US" ? "purple" : "black"}
      />
      <Button
        title="tr-TR"
        onPress={() => setLang("tr-TR")}
        color={lang === "tr-TR" ? "purple" : "black"}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    flexDirection: "row",
  },
});

export default ChangeLanguage;

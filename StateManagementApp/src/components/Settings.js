import { View, Text, Button } from "react-native";
import { useTheme } from "../context/ThemeContext";
import { useLang } from "../context/LangContext";

const Settings = () => {
  const { theme, setTheme } = useTheme();
  const { lang, setLang } = useLang();

  return (
    <View style={{ marginBottom: 100 }}>
      <Text>Settings</Text>
      <Text>Active Theme: {theme}</Text>
      <Text>Active Language: {lang}</Text>

      <Button
        title="Change Theme"
        onPress={() => setTheme((prev) => (prev === "dark" ? "light" : "dark"))}
      />

      <Button
        title="Change Language"
        onPress={() =>
          setLang((prev) => (prev === "tr-TR" ? "en-US" : "tr-TR"))
        }
      />
    </View>
  );
};

export default Settings;

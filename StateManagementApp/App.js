import { StyleSheet, View } from "react-native";
import { LangContextProvider } from "./src/context/LangContext";
import { ThemeContextProvider } from "./src/context/ThemeContext";
import Settings from "./src/components/Settings";
import ChangeLanguage from "./src/components/ChangeLanguage";
import ChangeTheme from "./src/components/ChangeTheme";
import Footer from "./src/components/Footer";

export default function App() {
  return (
    <View style={styles.container}>
      <LangContextProvider>
        <ThemeContextProvider>
          <Settings />
          <ChangeTheme />
          <ChangeLanguage />
        </ThemeContextProvider>
        <Footer />
      </LangContextProvider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

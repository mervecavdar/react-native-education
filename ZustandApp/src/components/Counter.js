import React from "react";
import { Button, Text, View } from "react-native";
import { useCounter } from "../context/CounterContext";

const Counter = () => {
  const { count, setCount } = useCounter();

  return (
    <View>
      <Text>{count}</Text>
      <Button
        title="Context Click"
        onPress={() => setCount((prev) => prev + 1)}
      />
    </View>
  );
};

export default Counter;

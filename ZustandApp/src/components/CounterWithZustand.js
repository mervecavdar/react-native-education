import React from "react";
import { View, Text, Button } from "react-native";
import useCounterStore from "../store/useCounterStore";

const CounterWithZustand = () => {
  const count = useCounterStore((state) => state.count);
  const increase = useCounterStore((state) => state.increase);

  return (
    <View>
      <Text>{count}</Text>
      <Button title="Zustand Click" onPress={increase} />
    </View>
  );
};

export default CounterWithZustand;

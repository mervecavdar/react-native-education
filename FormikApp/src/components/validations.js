import { object, string } from "yup";

const messages = {
  require: "This field is required.",
  email: "Enter a valid e-mail.",
};

const validations = object({
  name: string().required(messages.require),
  email: string().email(messages.email).required(messages.require),
  password: string()
    .min(6, "Please enter at least 6 characters.")
    .max(12, "Please enter a maximum of 12 characters.")
    .required(messages.require),
});

export default validations;

import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useFormik } from "formik";
import validations from "./validations";

const RegisterWithFormik = () => {
  const { values, errors, touched, handleChange, handleSubmit, handleBlur } =
    useFormik({
      initialValues: {
        name: "",
        email: "",
        password: "",
      },
      onSubmit: (_values) => console.log(_values),
      validationSchema: validations,
    });

  return (
    <View style={styles.container}>
      <View style={styles.input_wrapper}>
        <Text style={styles.label}>Name</Text>
        <TextInput
          style={styles.input}
          value={values.name}
          onChangeText={handleChange("name")}
          onBlur={handleBlur("name")}
        />
        {errors.name && touched.name && (
          <Text style={styles.error}>{errors.name}</Text>
        )}
      </View>

      <View style={styles.input_wrapper}>
        <Text style={styles.label}>E-mail</Text>
        <TextInput
          style={styles.input}
          autoCapitalize={false}
          keyboardType="email-address"
          value={values.email}
          onChangeText={handleChange("email")}
          onBlur={handleBlur("email")}
        />
        {errors.email && touched.email && (
          <Text style={styles.error}>{errors.email}</Text>
        )}
      </View>

      <View style={styles.input_wrapper}>
        <Text style={styles.label}>Password</Text>
        <TextInput
          style={styles.input}
          value={values.password}
          secureTextEntry
          onChangeText={handleChange("password")}
          onBlur={handleBlur("password")}
        />
        {errors.password && touched.password && (
          <Text style={styles.error}>{errors.password}</Text>
        )}
      </View>

      <View style={styles.input_wrapper}>
        <TouchableOpacity style={styles.btn} onPress={handleSubmit}>
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input_wrapper: {
    padding: 10,
  },
  label: {
    fontSize: 18,
  },
  input: {
    borderWidth: 1,
    padding: 8,
    borderColor: "#999",
    fontSize: 18,
  },
  btn: {
    alignItems: "center",
    backgroundColor: "lightblue",
    padding: 12,
    borderRadius: 10,
  },
  btnText: {
    fontSize: 18,
    color: "#333",
  },
  error: {
    color: "red",
    paddingTop: 3,
  },
});

export default RegisterWithFormik;

import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";

const Register = () => {
  const [form, setForm] = useState({ name: "", email: "", password: "" });

  const handleSubmit = () => {
    if (!form.name) {
      return false;
    }

    if (!form.email) {
      return false;
    }

    if (!form.password || form.password.length < 6) {
      return false;
    }

    console.log(form);
  };

  return (
    <View style={styles.container}>
      <View style={styles.input_wrapper}>
        <Text style={styles.label}>Name</Text>
        <TextInput
          style={styles.input}
          value={form.name}
          onChangeText={(text) => setForm((prev) => ({ ...prev, name: text }))}
        />
      </View>

      <View style={styles.input_wrapper}>
        <Text style={styles.label}>E-mail</Text>
        <TextInput
          style={styles.input}
          value={form.email}
          onChangeText={(text) => setForm((prev) => ({ ...prev, email: text }))}
          autoCapitalize={false}
          keyboardType="email-address"
        />
      </View>

      <View style={styles.input_wrapper}>
        <Text style={styles.label}>Password</Text>
        <TextInput
          style={styles.input}
          value={form.password}
          onChangeText={(text) =>
            setForm((prev) => ({ ...prev, password: text }))
          }
          secureTextEntry
        />
      </View>

      <View style={styles.input_wrapper}>
        <TouchableOpacity style={styles.btn} onPress={handleSubmit}>
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input_wrapper: {
    padding: 10,
  },
  label: {
    fontSize: 18,
  },
  input: {
    borderWidth: 1,
    padding: 8,
    borderColor: "#999",
    fontSize: 18,
  },
  btn: {
    alignItems: "center",
    backgroundColor: "lightblue",
    padding: 12,
    borderRadius: 10,
  },
  btnText: {
    fontSize: 18,
    color: "#333",
  },
});

export default Register;

import { SafeAreaView, StyleSheet } from "react-native";
import { StatusBar } from "expo-status-bar";
import Register from "./src/components/Register";
import RegisterWithFormik from "./src/components/RegisterWithFormik";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Register />
      <RegisterWithFormik />
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
});

import React, { useState } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import * as MediaLibrary from "expo-media-library";
import useStore from "../store/useStore";

const SnapButton = ({ camera }) => {
  const [loading, setLoading] = useState(false);
  const addPhoto = useStore((state) => state.addPhoto);

  const handleSnap = async () => {
    try {
      setLoading(true);

      if (camera) {
        const photo = await camera.current.takePictureAsync();
        setLoading(false);

        addPhoto(photo.uri);
        await MediaLibrary.saveToLibraryAsync(photo.uri);
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  return (
    <View style={styles.btn}>
      <TouchableOpacity
        style={styles.snapBtn}
        onPress={handleSnap}
        disabled={loading}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  snapBtn: {
    backgroundColor: "red",
    width: 70,
    height: 70,
    borderRadius: 50,
    borderWidth: 2,
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SnapButton;

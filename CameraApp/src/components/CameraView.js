import React, { useEffect, useRef } from "react";
import { View, StyleSheet, SafeAreaView } from "react-native";
import { Camera, CameraType } from "expo-camera";
import PhotoList from "./PhotoList";
import SnapButton from "./SnapButton";
import NoPermission from "./NoPermission";

const CameraView = () => {
  const camera = useRef(null);
  const [permission, requestPermission] = Camera.useCameraPermissions();

  useEffect(() => {
    requestPermission().then();
  }, []);

  if (!permission?.granted) {
    return <NoPermission />;
  }

  return (
    <View style={styles.container}>
      <Camera
        style={styles.camera}
        type={CameraType.back}
        ref={(ref) => (camera.current = ref)}
      >
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.bottomArea}>
            <PhotoList />
            <SnapButton camera={camera} />
          </View>
        </SafeAreaView>
      </Camera>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  safeArea: {
    flex: 1,
    justifyContent: "flex-end",
  },
  bottomArea: {
    padding: 10,
  },
});

export default CameraView;

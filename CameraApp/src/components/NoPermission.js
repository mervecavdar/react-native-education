import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
  Platform,
} from "react-native";

const NoPermission = () => {
  const handleClick = () => {
    if (Platform.OS === "ios") {
      return Linking.openURL("app-settings:");
    }
    return Linking.openSettings();
  };

  return (
    <View style={styles.container}>
      <Text>NoPermission</Text>
      <TouchableOpacity onPress={handleClick}>
        <Text>Open Settings</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default NoPermission;

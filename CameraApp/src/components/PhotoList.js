import React from "react";
import { View, StyleSheet, FlatList } from "react-native";
import PhotoItem from "./PhotoItem";
import useStore from "../store/useStore";

const PhotoList = () => {
  const photos = useStore((state) => state.photos);

  if (!photos.length) {
    return null;
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={photos}
        renderItem={({ item }) => <PhotoItem uri={item} />}
        keyExtractor={(_, index) => index.toString()}
        horizontal
        style={{
          paddingBottom: 12,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(128, 128, 128, 0.3)",
    padding: 10,
    marginBottom: 10,
    flexDirection: "row",
    borderRadius: 12,
    marginHorizontal: 15,
  },
});

export default PhotoList;

import React from "react";
import { Image, StyleSheet } from "react-native";

const PhotoItem = ({ uri }) => {
  return <Image style={styles.image} source={{ uri }} />;
};

const styles = StyleSheet.create({
  image: {
    width: 60,
    height: 60,
    borderWidth: 2,
    marginRight: 4,
    borderRadius: 10,
  },
});

export default PhotoItem;

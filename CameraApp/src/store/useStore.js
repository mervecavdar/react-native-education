import create from "zustand";

const useStore = (set) => ({
  photos: [],
  addPhoto: (item) => set((state) => ({ photos: [item, ...state.photos] })),
});

export default create(useStore);

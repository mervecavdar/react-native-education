import * as React from "react";
import Router from "./src/Router";
import "react-native-gesture-handler";

function App() {
  return <Router />;
}

export default App;

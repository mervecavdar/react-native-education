import React from "react";
import { TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Ionicons from "@expo/vector-icons/Ionicons";

const ToggleDrawer = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
      <Ionicons name={"menu"} size={28} color={"#000"} />
    </TouchableOpacity>
  );
};

export default ToggleDrawer;

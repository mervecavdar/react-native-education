import React, { useEffect, useState } from "react";
import { View, StyleSheet, FlatList, Modal } from "react-native";
import UserItem from "./UserItem";
import AddButton from "../../components/AddButton";
import AddUserModal from "./AddUserModal";

const Users = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then(setUsers);
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <AddButton onPress={() => setModalVisible(true)} title="Update count" />
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <FlatList
        data={users}
        renderItem={({ item }) => <UserItem item={item} />}
        keyExtractor={(item) => item.id}
      />

      <Modal
        presentationStyle="pageSheet"
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <AddUserModal setUsers={setUsers} setModalVisible={setModalVisible} />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Users;

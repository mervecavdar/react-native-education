import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    marginBottom: 60,
  },
  input: {
    borderWidth: 1,
    padding: 10,
    borderColor: "#999",
    fontSize: 20,
  },
  error: {
    color: "red",
    paddingTop: 4,
    paddingBottom: 7,
  },
});

export default styles;

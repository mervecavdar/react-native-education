import { object, string } from "yup";

const validations = object({
  name: string().required(),
  email: string().email().required(),
  website: string().url().nullable(),
});

export default validations;

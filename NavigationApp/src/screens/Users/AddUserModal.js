import React from "react";
import { View, Text, TextInput, Button } from "react-native";
import { useFormik } from "formik";
import styles from "./styles";
import validationSchema from "./validations";

const AddUserModal = ({ setUsers, setModalVisible }) => {
  const { values, errors, touched, handleChange, handleSubmit, handleBlur } =
    useFormik({
      initialValues: {
        name: "",
        email: "",
        website: "",
      },
      onSubmit: (_values) => {
        setModalVisible(false);
        setUsers((prev) => [
          { ...values, id: ~~(Math.random() * 1000) },
          ...prev,
        ]);
      },
      validationSchema,
    });

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Add a User</Text>

      <TextInput
        style={styles.input}
        placeholder="name"
        value={values.name}
        onChangeText={handleChange("name")}
        onBlur={handleBlur("name")}
      />
      {errors.name && touched.name && (
        <Text style={styles.error}>{errors.name}</Text>
      )}

      <TextInput
        style={styles.input}
        placeholder="e-mail"
        keyboardType="email-address"
        value={values.email}
        onChangeText={handleChange("email")}
        onBlur={handleBlur("email")}
        autoCapitalize={false}
      />
      {errors.email && touched.email && (
        <Text style={styles.error}>{errors.email}</Text>
      )}

      <TextInput
        style={styles.input}
        placeholder="website"
        value={values.website}
        onChangeText={handleChange("website")}
        onBlur={handleBlur("website")}
        autoCapitalize={false}
      />
      {errors.website && touched.website && (
        <Text style={styles.error}>{errors.website}</Text>
      )}

      <Button title="Save" onPress={handleSubmit} />
    </View>
  );
};

export default AddUserModal;

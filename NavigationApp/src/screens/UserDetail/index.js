import React from "react";
import { View, Text } from "react-native";

const UserDetail = ({ route }) => {
  const { item } = route.params;

  return (
    <View>
      <Text>{JSON.stringify(item, null, 4)}</Text>
    </View>
  );
};

export default UserDetail;

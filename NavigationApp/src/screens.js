const screens = {
  home: {
    name: "home",
    title: "Home",
  },
  users: {
    name: "users",
    title: "Users",
  },
  user_detail: {
    name: "user_detail",
    title: "User Detail",
  },
};

export default screens;

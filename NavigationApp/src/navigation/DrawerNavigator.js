import { createDrawerNavigator } from "@react-navigation/drawer";
import { UsersStackNavigator } from "./StackNavigator";
import BottomTabNavigator from "./BottomTabNavigator";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerType: "slide",
      }}
    >
      <Drawer.Screen name="Home" component={BottomTabNavigator} />
      <Drawer.Screen name="Users" component={UsersStackNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home";
import Users from "../screens/Users";
import UserDetail from "../screens/UserDetail";
import Settings from "../screens/Settings";
import SettingsDetail from "../screens/SettingsDetail";
import ToggleDrawer from "../components/ToggleDrawer";

const screenOptionStyle = {
  headerBackTitle: "Back",
  headerLeft: () => <ToggleDrawer />,
};

const Stack = createNativeStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="home_screen"
        component={Home}
        options={{ title: "Home" }}
      />
    </Stack.Navigator>
  );
};

const UsersStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="users_home"
        component={Users}
        options={{ title: "Users" }}
      />
      <Stack.Screen
        name="user_detail"
        component={UserDetail}
        options={({ route }) => ({ title: route.params.item.name })}
      />
    </Stack.Navigator>
  );
};

const SettingsStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="settings_home"
        component={Settings}
        options={{ title: "Settings" }}
      />
      <Stack.Screen
        name="settings_detail"
        component={SettingsDetail}
        options={{ title: "Settings Detail" }}
      />
    </Stack.Navigator>
  );
};

export { MainStackNavigator, UsersStackNavigator, SettingsStackNavigator };

import { useState, useCallback } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import Footer from "./src/components/Footer";

export default function App() {
  const [count, setCount] = useState(0);

  const increase = useCallback((amount) => {
    setCount((prev) => prev + amount);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{count}</Text>
      <Button title="Increase" onPress={() => increase(4)} />
      <Footer increase={increase} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    padding: 20,
    paddingTop: 40,
  },
  text: {
    fontSize: 28,
  },
});

import React, { useState } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

const Form = () => {
  const [text, setText] = useState("");

  return (
    <View>
      <Text>{text}</Text>
      <TextInput style={styles.input} value={text} onChangeText={setText} />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderWidth: 2,
    width: "100%",
    padding: 10,
    fontSize: 20,
  },
});

export default Form;

import React from "react";
import { View, Text, StyleSheet } from "react-native";

const User = ({ data }) => {
  return (
    <View style={styles.container}>
      <Text>User</Text>
      <Text>{JSON.stringify(data, null, 2)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    flex: 1,
  },
});

export default React.memo(User);

import React from "react";
import { View, Text } from "react-native";

const List = () => {
  const numbers = new Array(100000)
    .fill()
    .map(() => Math.floor(Math.random() * 100));

  return (
    <View style={{ marginTop: 30 }}>
      <Text>{JSON.stringify(numbers)}</Text>
    </View>
  );
};

export default React.memo(List);

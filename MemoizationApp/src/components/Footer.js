import React from "react";
import { View, Text, Button } from "react-native";

const Footer = ({ increase }) => {
  console.log("footer re-render");

  return (
    <View style={{ marginTop: 100 }}>
      <Text>Footer</Text>
      <Button title="Increase" onPress={() => increase(10)} />
    </View>
  );
};

export default React.memo(Footer);

import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";

const RandomNumbers = ({ counter }) => {
  const numbers = new Array(100000).fill().map(() => ~~(Math.random() * 100));

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text>Counter: {counter}</Text>
        <Text>{JSON.stringify(numbers)}</Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopWidth: 1,
    paddingTop: 30,
    flex: 6,
  },
});

export default React.memo(RandomNumbers);
